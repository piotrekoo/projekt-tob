import datamodel.AccountData;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class AccountWindow extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JLabel accountID;
    private JLabel balanceLabel;
    private JTextField textField1;
    private JTextField textField2;
    private JButton withdrawalButton;
    private JButton depositButton;
    private String loggedUser;
    private AccountData accountData;

    public AccountWindow(String username) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        setUserName(username);
        accountData = getAccountData();

        refreshBalance();

        withdrawalButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    accountData.withdraw(Double.parseDouble(textField1.getText()));
                } catch (NumberFormatException e1) {
                    JOptionPane.showMessageDialog(getParent(), "Amount format is incorrect");
                } catch (Exception e2) {
                    JOptionPane.showMessageDialog(getParent(), e2.getMessage());
                } finally {
                    refreshBalance();
                }
            }
        });

        depositButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    accountData.deposit(Double.parseDouble(textField2.getText()));
                } catch (NumberFormatException e1) {
                    JOptionPane.showMessageDialog(getParent(), "Amount format is incorrect");
                } catch (Exception e2) {
                    JOptionPane.showMessageDialog(getParent(), e2.getMessage());
                } finally {
                    refreshBalance();
                }
            }
        });
    }

    private void setUserName(String username) {
        loggedUser = username;
        accountID.setText(accountID.getText() + " " + loggedUser);
    }

    private void onOK() {
        dispose();
    }

    /* public static void main(String[] args) {
        AccountWindow dialog = new AccountWindow(args[0]);
        dialog.pack();
        dialog.setVisible(true);
    } */

    public void getGUIon() {
        this.pack();
        this.setVisible(true);
    }

    private void createUIComponents() {
        accountID = new JLabel("Account ID: ");
    }

    private AccountData getAccountData() {
        try {
            //use buffering
            InputStream file = new FileInputStream(loggedUser + ".ser");
            InputStream buffer = new BufferedInputStream(file);
            ObjectInput input = new ObjectInputStream(buffer);
            try {
                //deserialize the List
                return (AccountData) input.readObject();
            } finally {
                input.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new AccountData(loggedUser);
    }

    private void saveAccountData() {
        try {
            //use buffering
            OutputStream file = new FileOutputStream(loggedUser + ".ser");
            OutputStream buffer = new BufferedOutputStream(file);
            ObjectOutput output = new ObjectOutputStream(buffer);
            try {
                output.writeObject(accountData);
            } finally {
                output.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void refreshBalance() {
        balanceLabel.setText(String.valueOf(accountData.getBalance()));
        saveAccountData();
    }
}
