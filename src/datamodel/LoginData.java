package datamodel;

import java.io.Serializable;

/**
 * Created by piotr on 17.01.15.
 */
public class LoginData implements Serializable{
    public LoginData (String login, String password)
    {
        this.login = login;
        this.password = password;
    }

    private String login;
    private String password;

    public String getLogin() {
        return login;
    }

    public boolean checkPassword(String input) {
        return (input.equals(this.password));
    }
}
