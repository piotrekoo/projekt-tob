package datamodel;

import java.io.Serializable;

public class AccountData implements Serializable {
    public AccountData(String userName){
        this.userName = userName;
        this.balance = 0d;
    }

    String userName;
    double balance;

    public double getBalance()
    {
        return balance;
    }

    public void deposit (double amount) throws Exception {
        if(amount <= 0d) {
            throw new Exception("You cannot deposit negatives");
        }
        balance += amount;
    }

    public void withdraw (double amount) throws Exception {
        if(amount <= 0d) {
            throw new Exception("You cannot withdraw negatives");
        }
        if (amount > balance) {
            throw new Exception("Your funds are nonsufficient");
        }
        balance -= amount;
    }
}
