import datamodel.LoginData;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class MainWindow extends JDialog {
    private JPanel contentPane;
    private JPanel loginPanel;
    private JPanel registrationPanel;
    private JLabel loginLabel;
    private JTextField LoginIdTextField;
    private JPasswordField LoginpasswordField;
    private JLabel registrationLabel;
    private JTextField RegistrationTextField;
    private JPasswordField RegistrationpasswordField1;
    private JPasswordField RegistrationpasswordField2;
    private JButton registerButton;
    private JButton loginButton;
    private JButton buttonOK;

    private ArrayList<LoginData> loginDatas;

    public MainWindow() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (LoginData data : loginDatas) {
                    if(data.getLogin().equals(LoginIdTextField.getText()))
                    {
                        if (data.checkPassword(String.valueOf(LoginpasswordField.getPassword())))
                        {
                            JOptionPane.showMessageDialog(getParent(), "Login succeeded");
                            AccountWindow accountWindow = new AccountWindow(data.getLogin());
                            accountWindow.getGUIon();
                            return;
                        }
                    }
                }
                JOptionPane.showMessageDialog(getParent(), "Login failed");
            }
        });
        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                for (LoginData loginData : loginDatas) {
                    if (loginData.getLogin().equals(RegistrationTextField.getText()))
                    {
                        JOptionPane.showMessageDialog(getParent(), "Login is already taken");
                        return;
                    }
                }

                // Check is passwords match
                if(!Arrays.equals(RegistrationpasswordField1.getPassword(), RegistrationpasswordField2.getPassword())) {
                    JOptionPane.showMessageDialog(getParent(), "Passwords don't match");
                    return;
                }

                if(RegistrationpasswordField1.getPassword().length < 1) {
                    JOptionPane.showMessageDialog(getParent(), "Password cannot be empty");
                    return;
                }

                loginDatas.add(new LoginData(RegistrationTextField.getText(), String.valueOf(RegistrationpasswordField1.getPassword())));
                saveLoginData();
                JOptionPane.showMessageDialog(getParent(), "Account registered");
            }
        });
    }

    public static void main(String[] args) {
        MainWindow dialog = new MainWindow();
        dialog.loginDatas = dialog.getLoginData();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    private ArrayList<LoginData> getLoginData() {
        try{
            //use buffering
            InputStream file = new FileInputStream("login.ser");
            InputStream buffer = new BufferedInputStream(file);
            ObjectInput input = new ObjectInputStream(buffer);
            try{
                //deserialize the List
                return (ArrayList<LoginData>)input.readObject();
            }
            finally{
                input.close();
            }
        }
        catch(Exception ex){
            return new ArrayList<LoginData>();
        }
    }

    private void saveLoginData() {
        try{
            //use buffering
            OutputStream file = new FileOutputStream("login.ser");
            OutputStream buffer = new BufferedOutputStream(file);
            ObjectOutput output = new ObjectOutputStream(buffer);
            try{
                output.writeObject(loginDatas);
            }
            finally{
                output.close();
            }
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
    }

    private void createUIComponents() {
        loginLabel = new JLabel("Login");
        loginLabel.setFont(loginLabel.getFont().deriveFont(32.0f));
        registrationLabel = new JLabel("Registration");
        registrationLabel.setFont(registrationLabel.getFont().deriveFont(32.0f));
    }
}
