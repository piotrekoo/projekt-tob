import datamodel.LoginData;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by piotr on 17.01.15.
 */
public class test {
    public static void main(String[] args) {
        ArrayList<LoginData> loginDataList = new ArrayList<LoginData>();

        loginDataList.add(new LoginData("login1", "pass1"));
        loginDataList.add(new LoginData("login2", "pass2"));
        loginDataList.add(new LoginData("login3", "pass3"));

        try {
            OutputStream file = new FileOutputStream("login.ser");
            OutputStream buffer = new BufferedOutputStream(file);
            ObjectOutput output = new ObjectOutputStream(buffer);
            output.writeObject(loginDataList);
            output.close();
        }
        catch(IOException ex){
            ex.printStackTrace();
        }


        try{
            //use buffering
            InputStream file = new FileInputStream("login.ser");
            InputStream buffer = new BufferedInputStream(file);
            ObjectInput input = new ObjectInputStream (buffer);
            try{
                //deserialize the List
                List<LoginData> recoveredLogin = (List<LoginData>)input.readObject();
                //display its data
                for(LoginData data: recoveredLogin){
                    System.out.println(data.getLogin());
                }
            }
            finally{
                input.close();
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
